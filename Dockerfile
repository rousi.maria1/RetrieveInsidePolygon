FROM java:8

# Install maven
RUN apt-get -y update && apt-get install -y maven

WORKDIR /code

# Prepare by downloading dependencies
ADD pom.xml /code/pom.xml
ADD server.xml /code/server.xml

# Adding source, compile and package into a fat jar
ADD src /code/src
RUN ["mvn", "package"]

ENV GRAPHDB_ADDRESS default_env_value

EXPOSE 7204
CMD ["mvn", "tomcat7:run"]