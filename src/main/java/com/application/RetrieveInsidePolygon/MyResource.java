
package com.application.RetrieveInsidePolygon;

import java.io.IOException;
import java.text.ParseException;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.rdf4j.model.Model;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

/** Example resource class hosted at the URI path "/myresource"
 */
//@Path("/retrieve")
@Path("")
public class MyResource {
    
    /** Method processing HTTP GET requests, producing "text/plain" MIME media
     * type.
     * @return String that will be send back as a response of type "text/plain".
     * @throws ParseException 
     */
	@POST
	    @Consumes(MediaType.APPLICATION_JSON)
	    public Response createDataInJSON(String data) throws JsonSyntaxException, JsonIOException, IOException, InterruptedException, ParseException { 
	    	 String uuid = UUID.randomUUID().toString().replaceAll("-", "");
	    	 String javaHome = System.getenv("GRAPHDB_ADDRESS");	 
		String currentLine, allfile="";
		String text=data;
		System.out.println(text);
		
		// Check if input is empty
		System.out.println(text.isEmpty());
	
		if(!text.isEmpty()) {
			
				 String res = Mapping.polygonQueryExecution(text.toString(), javaHome);
				 return Response.status(201).entity(res).build();
		}
		return null;
	}
			 
}
