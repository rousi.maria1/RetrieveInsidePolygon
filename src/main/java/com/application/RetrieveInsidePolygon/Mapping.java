package com.application.RetrieveInsidePolygon;

import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.http.HTTPRepository;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Mapping {
	public static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
    public static final Charset UTF_8 = Charset.forName("UTF-8");
	public static String polygonQueryExecution(String jsonob, String server) throws ParseException {
		Gson gson = new Gson();
        JsonElement json = gson.fromJson(jsonob, JsonElement.class);
        JsonObject jobject = json.getAsJsonObject();
        String polygon=jobject.get("geometry").getAsString().replaceAll("POLYGON", "").replaceAll("[()]", "");
        //2019-11-17T04:58:27.000
      //  DateTimeFormatter dtf  = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
     //   ZonedDateTime     zdt  = ZonedDateTime.parse(jobject.get("start_date").getAsString(),dtf);  
      //  Date st_date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(jobject.get("start_date").getAsString());
        //ZonedDateTime     zdt  = ZonedDateTime.parse(jobject.get("start_date").getAsString(),st_date);  
        System.out.println("Timestamp start::"+Instant.parse(jobject.get("start_date").getAsString()).toEpochMilli());
        //Instant.parse( "2014-09-01T19:22:43.000Z").getEpochSecond();
      //  long instant = Instant.parse( jobject.get("start_date").getAsString()).getEpochSecond();
       // long millisecondsSinceUnixEpoch = instant.toEpochMilli() ;
        String start=String.valueOf(Instant.parse(jobject.get("start_date").getAsString()).toEpochMilli());
      //  DateTimeFormatter dtfe  = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
      //  ZonedDateTime     zdte  = ZonedDateTime.parse(jobject.get("end_date").getAsString(),dtfe);   
      //  Date en_date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(jobject.get("end_date").getAsString());
       // System.out.println("Timestamp end::"+en_date);
        //System.out.println(zdt.toInstant().toEpochMilli());
       
        String end=String.valueOf(Instant.parse(jobject.get("end_date").getAsString()).toEpochMilli());
		
		/*String query="PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>\r\n" + 
				"PREFIX ogc: <http://www.opengis.net/ont/geosparql#>\r\n" + 
				"PREFIX geof: <http://www.opengis.net/def/function/geosparql/>\r\n" + 
				"PREFIX geo: <http://www.opengis.net/ont/geosparql#>\r\n" + 
				"PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>\r\n" + 
				"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\r\n" + 
				"PREFIX ta: <https://eopen-project.eu/ontologies/tweet-annotations#>\r\n" + 
				"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\r\n" + 
				"PREFIX oa: <http://www.w3.org/ns/oa#>\r\n" + 
				"PREFIX time: <https://www.w3.org/2006/time#> "+
				"select ?event ?lang ?distance where {\r\n" + 
				"    ?r oa:hasBody ?b.\r\n" + 
				"    ?b rdfs:label ?event.\r\n" + 
				"    ?b ta:location ?a.\r\n" + 
				"    ?a ta:hasPoint ?s. \r\n" + 
				"    ?a rdfs:label ?lbl.\r\n" + 
				"    ?s ogc:asWKT ?o .\r\n" + 
				"    ?r oa:hasTarget ?t.\r\n" + 
				"    ?t ta:language ?lang.\r\n" + 
				" \r\n" + 
				"BIND(geof:distance(?o, '''<http://www.opengis.net/def/crs/OGC/1.3/CRS84>\r\n" + 
				"            Polygon (("+polygon+"))'''^^geo:wktLiteral, uom:metre) AS ?distance)\r\n" + 
				"    FILTER(?distance=\"0.0\"^^xsd:double)\r\n" + 
				"   FILTER(xsd:double(?ts)>=\""+start+"\"^^xsd:double && xsd:double(?ts)<=\""+end+"\"^^xsd:double)\r\n" + 
				"} \r\n" + 
				"\r\n" + 
				"";*/
       
		String query="PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>\r\n" + 
				"PREFIX ogc: <http://www.opengis.net/ont/geosparql#>\r\n" + 
				"PREFIX geof: <http://www.opengis.net/def/function/geosparql/>\r\n" + 
				"PREFIX geo: <http://www.opengis.net/ont/geosparql#>\r\n" + 
				"PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>\r\n" + 
				"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\r\n" + 
				"PREFIX ta: <https://eopen-project.eu/ontologies/tweet-annotations#>\r\n" + 
				"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\r\n" + 
				"PREFIX oa: <http://www.w3.org/ns/oa#>\r\n" + 
				"PREFIX time: <https://www.w3.org/2006/time#> \r\n" + 
				"select distinct ?r where {\r\n" + 
				"    ?r oa:hasBody ?b.\r\n" + 
				"    ?b rdfs:label ?event.\r\n" + 
				"    ?b ta:location ?a.\r\n" + 
				"    ?a ta:hasPoint ?s.\r\n" + 
				"    ?a rdfs:label ?lbl.\r\n" + 
				"    ?s ogc:asWKT ?o .\r\n" + 
				"    ?r oa:hasTarget ?t.\r\n" + 
				"    ?t ta:language ?lang.\r\n" + 
				"    ?t ta:rate ?rate.\r\n" + 
				"    ?t ta:score ?score.\r\n" + 
				"    ?t ta:timestamp ?ts.\r\n" + 
				"\r\n" + 
				"BIND(geof:distance(?o, '''<http://www.opengis.net/def/crs/OGC/1.3/CRS84>\r\n" + 
				"            Polygon (("+polygon+"))'''^^geo:wktLiteral, uom:metre) AS ?distance)\r\n" + 
				"    FILTER(?distance=\"0.0\"^^xsd:double)\r\n" + 
				"   FILTER(xsd:double(?ts)>=\""+start+"\"^^xsd:double && xsd:double(?ts)<=\""+end+"\"^^xsd:double)\r\n" + 
				"}\r\n" + 
				"";
		Repository twitterRepository = new HTTPRepository(server, "TwitterRepository");
		twitterRepository.initialize();
		RepositoryConnection twitterConnection = twitterRepository.getConnection();
        
		System.out.println(query);
          
        TupleQuery query1 = twitterConnection.prepareTupleQuery(query);
        ArrayList <String> ev_arr = new ArrayList <String> ();
        JSONObject event = new JSONObject ();
        JSONArray ev = new JSONArray ();
        JSONObject results = new JSONObject ();
        JSONArray features = new JSONArray ();
        JSONObject result = new JSONObject ();
        TupleQueryResult qresult1 = query1.evaluate();
        results.put("type", "FeatureCollection");
        while (qresult1.hasNext()) {
            BindingSet st1 = qresult1.next();
            /*if(st1.hasBinding("event") && st1.hasBinding("lang") && st1.hasBinding("distance")) {
            	
            	event = new JSONObject ();
            	event.put("event", st1.getValue("event").stringValue());
            	event.put("language", st1.getValue("lang").stringValue());
            	ev.add(event);          	
            }*/
            if(st1.hasBinding("r")) {
            	
            	
            	ev_arr.add(st1.getValue("r").stringValue());
            	         	
            }
        }
        for (int i=0; i<ev_arr.size(); i++) {
        	
        	query="PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>\r\n" + 
    				"PREFIX ogc: <http://www.opengis.net/ont/geosparql#>\r\n" + 
    				"PREFIX geof: <http://www.opengis.net/def/function/geosparql/>\r\n" + 
    				"PREFIX geo: <http://www.opengis.net/ont/geosparql#>\r\n" + 
    				"PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>\r\n" + 
    				"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\r\n" + 
    				"PREFIX ta: <https://eopen-project.eu/ontologies/tweet-annotations#>\r\n" + 
    				"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\r\n" + 
    				"PREFIX oa: <http://www.w3.org/ns/oa#>\r\n" + 
    				"PREFIX time: <https://www.w3.org/2006/time#> \r\n" + 
    				"select * where {\r\n" + 
    				" BIND (<"+ev_arr.get(i)+"> AS ?r)"+
    				"    ?r oa:hasBody ?b.\r\n" + 
    				"    ?b rdfs:label ?event.\r\n" + 
    				"    ?b ta:location ?a.\r\n" + 
    				"    ?a ta:hasPoint ?s.\r\n" + 
    				"    ?a rdfs:label ?lbl.\r\n" + 
    				"    ?s ogc:asWKT ?o .\r\n" + 
    				"    ?r oa:hasTarget ?t.\r\n" + 
    				"    ?t ta:language ?lang.\r\n" + 
    				"    ?t ta:rate ?rate.\r\n" + 
    				"    ?t ta:score ?score.\r\n" + 
    				"    ?t ta:timestamp ?ts.\r\n" + 
    				"\r\n" + 
    				"BIND(geof:distance(?o, '''<http://www.opengis.net/def/crs/OGC/1.3/CRS84>\r\n" + 
    				"            Polygon (("+polygon+"))'''^^geo:wktLiteral, uom:metre) AS ?distance)\r\n" + 
    				"    FILTER(?distance=\"0.0\"^^xsd:double)\r\n" + 
    				"   FILTER(xsd:double(?ts)>=\""+start+"\"^^xsd:double && xsd:double(?ts)<=\""+end+"\"^^xsd:double)\r\n" + 
    				"}\r\n" + 
    				"";
        	
        //	System.out.println("new:"+query);
        	 query1 = twitterConnection.prepareTupleQuery(query);
   	      JSONObject pr = new JSONObject ();
   	   JSONObject geo = new JSONObject ();
   	        qresult1 = query1.evaluate();
   	        while (qresult1.hasNext()) {
   	            BindingSet st1 = qresult1.next();
   	            if(st1.hasBinding("ts") && st1.hasBinding("event") && st1.hasBinding("lang") && st1.hasBinding("score") && st1.hasBinding("rate") && st1.hasBinding("lbl") && st1.hasBinding("o") 
   	            		&& st1.hasBinding("distance")) {
            	
            	event = new JSONObject ();
            	event.put("type", "Feature");
            	event.put("id", st1.getValue("r").stringValue().replaceAll("https://eopen-project.eu/ontologies/tweet-annotations#Event_Annotation_", ""));
            	
            	pr.put("timestamp", st1.getValue("ts").stringValue());
            	pr.put("usecase", st1.getValue("event").stringValue());
            	
            	pr.put("language", st1.getValue("lang").stringValue());
            	
            	pr.put("score", st1.getValue("score").stringValue());
            	pr.put("change", st1.getValue("rate").stringValue());
            	
            	pr.put("location", st1.getValue("lbl").stringValue());
            	String point = st1.getValue("o").stringValue();
            	point = point.replaceAll("<http://www.opengis.net/def/crs/OGC/1.3/CRS84> ", "");
            	point = point.replaceAll("POINT", "");
            	point = point.replaceAll("ogc:wktLiteral", "");
            	point = point.replaceAll("[()]", "");
            	point = point.replaceAll("[^\\x00-\\x7F]", "");
            	point = point.replaceAll("^", "");
            	point = point.replaceAll("&#94;","");
            	String[] splited = point.split("\\s+");
            	JSONArray coords=new JSONArray();
            	if(splited.length==2) {
            		
            		coords.add(splited[0].replaceAll("[^\\d.]", ""));
            		coords.add(splited[1].replaceAll("[^\\d.]", ""));
            	}
            	geo.put("type", "Point");
            	geo.put("coordinates", coords);
            	geo.put("geometry_name", st1.getValue("s").stringValue().replaceAll("https://eopen-project.eu/ontologies/tweet-annotations#", ""));
            	        	
            }
   	        }
   	     event.put("geometry", geo);
   	  geo = new JSONObject ();
   	     query="PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>\r\n" + 
   	     		"PREFIX ogc: <http://www.opengis.net/ont/geosparql#>\r\n" + 
   	     		"PREFIX geof: <http://www.opengis.net/def/function/geosparql/>\r\n" + 
   	     		"PREFIX geo: <http://www.opengis.net/ont/geosparql#>\r\n" + 
   	     		"PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>\r\n" + 
   	     		"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\r\n" + 
   	     		"PREFIX ta: <https://eopen-project.eu/ontologies/tweet-annotations#>\r\n" + 
   	     		"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\r\n" + 
   	     		"PREFIX oa: <http://www.w3.org/ns/oa#>\r\n" + 
   	     		"PREFIX time: <https://www.w3.org/2006/time#> \r\n" + 
   	     		"select distinct ?keyword where {\r\n" + 
   	     	   " BIND (<"+ev_arr.get(i)+"> AS ?r)"+
   	     		"    ?r oa:hasBody ?b.\r\n" + 
   	     		"    ?b ta:hasKeywords ?key.\r\n" + 
   	     		"    ?key rdfs:label ?keyword .\r\n" + 
   	     		"}";
     	
     	
     	 query1 = twitterConnection.prepareTupleQuery(query);
	      
	       JSONArray keywords = new JSONArray ();
	
	        qresult1 = query1.evaluate();
	        while (qresult1.hasNext()) {
	            BindingSet st1 = qresult1.next();
	            if(st1.hasBinding("keyword")) {
         	
	            	keywords.add(st1.getValue("keyword").stringValue());
   	
	            }
	        }
	        pr.put("keywords", keywords);
        	pr.put("feature_type", "has_events");
        	event.put("properties", pr);
	        //ev.add(event);  
        	//result.put("has_events", ev);
        	features.add(event);
        	event = new JSONObject();
        	pr = new JSONObject();
        }
        
		//event
        query="PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>\r\n" + 
				"PREFIX ogc: <http://www.opengis.net/ont/geosparql#>\r\n" + 
				"PREFIX geof: <http://www.opengis.net/def/function/geosparql/>\r\n" + 
				"PREFIX geo: <http://www.opengis.net/ont/geosparql#>\r\n" + 
				"PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>\r\n" + 
				"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\r\n" + 
				"PREFIX ta: <https://eopen-project.eu/ontologies/tweet-annotations#>\r\n" + 
				"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\r\n" + 
				"PREFIX oa: <http://www.w3.org/ns/oa#>\r\n" + 
				"PREFIX time: <https://www.w3.org/2006/time#> "+
        		"select * where {\r\n" + 
        		"    ?r oa:hasBody ?b.\r\n" + 
        		"    ?r oa:hasTarget ?target.\r\n" + 
        		"    ?target ta:location ?a.\r\n" + 
        		" ?target ta:hasId ?id. \r\n"+
        		"    ?a ta:hasPoint ?s. \r\n" + 
        		"    ?a rdfs:label ?lbl.\r\n" + 
        		"    ?s ogc:asWKT ?o .\r\n" + 
        		"    ?b ta:hasUseCase ?usecase.\r\n" + 
        		"    ?b ta:hasLanguage ?lang.\r\n" + 
        		"    ?b time:inXSDDateTimeStamp ?ts." + 
        		"BIND(geof:distance(?o, '''<http://www.opengis.net/def/crs/OGC/1.3/CRS84>\r\n" + 
				"            Polygon (("+polygon+"))'''^^geo:wktLiteral, uom:metre) AS ?distance)\r\n" + 
				"    FILTER(?distance=\"0.0\"^^xsd:double)\r\n" + 
				"   FILTER(xsd:double(?ts)>=\""+start+"\"^^xsd:double && xsd:double(?ts)<=\""+end+"\"^^xsd:double)\r\n" + 
				"} \r\n" + 
				"\r\n" + 
				"";
        //System.out.println(query);
        
		  query1 = twitterConnection.prepareTupleQuery(query);
	      
		  JSONObject g = new JSONObject (); 
		  JSONArray tw = new JSONArray ();
	        JSONObject tweet = new JSONObject ();
	        JSONObject tprop = new JSONObject ();
	        qresult1 = query1.evaluate();
	        while (qresult1.hasNext()) {
	            BindingSet st1 = qresult1.next();
	            if(st1.hasBinding("usecase") && st1.hasBinding("lang") && st1.hasBinding("id") && st1.hasBinding("ts") && st1.hasBinding("lbl") && st1.hasBinding("o")) {
	            	
	            	tweet = new JSONObject ();
	            	tweet.put("type", "Feature");
	            	tweet.put("id", st1.getValue("s").stringValue());
	            	
	            	tprop.put("feature_type","has_tweets");
	            	tprop.put("usecase", st1.getValue("usecase").stringValue());
	            	tprop.put("language", st1.getValue("lang").stringValue());
	            	tprop.put("id", st1.getValue("id").stringValue().replaceAll("https://eopen-project.eu/ontologies/tweet-annotations#Tweet_Annotation_", "").replaceAll("https://eopen-project.eu/ontologies/tweet-annotations#", ""));
	            	tprop.put("timestamp", st1.getValue("ts").stringValue());
	            	tprop.put("location", st1.getValue("lbl").stringValue());
	            	tweet.put("properties", tprop);
	            	String point = st1.getValue("o").stringValue();
	            	point = point.replaceAll("<http://www.opengis.net/def/crs/OGC/1.3/CRS84> ", "");
	            	point = point.replaceAll("POINT", "");
	            	point = point.replaceAll("<http://www.opengis.net/ont/geosparql#wktLiteral>", "");
	            	point = point.replaceAll("ogc:wktLiteral", "");
	            	point = point.replaceAll("[()]", "");
	            	point = point.replaceAll("[^\\x00-\\x7F]", "");
	            	
	            	point = point.replaceAll("^", "");
	            	point = point.replaceAll("&#94;","");
	            	
	            	String[] splited = point.split("\\s+");
	            	JSONArray coords=new JSONArray();
	            	if(splited.length==2) {
	            		
	            		coords.add(splited[0].replaceAll("[^\\d.]", ""));
	            		coords.add(splited[1].replaceAll("[^\\d.]", ""));
	            	}
	            	g.put("type", "Point");
	            	g.put("coordinates", coords);
	            	g.put("geometry_name", st1.getValue("s").stringValue().replaceAll("https://eopen-project.eu/ontologies/tweet-annotations#", ""));
	            	tweet.put("geometry", g);
	            	g = new JSONObject();
	            	
	            	//tweet.put("point", coords);
	            	//tw.add(tweet);    
	            	//tw.add(st1.getValue("id").stringValue());   
	            	
	    	        features.add(tweet);
	    	        tweet = new JSONObject();
	    	        tprop = new JSONObject();
	            }
	        }
	        
		 /*query="PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>\r\n" + 
				"PREFIX ogc: <http://www.opengis.net/ont/geosparql#>\r\n" + 
				"PREFIX geof: <http://www.opengis.net/def/function/geosparql/>\r\n" + 
				"PREFIX geo: <http://www.opengis.net/ont/geosparql#>\r\n" + 
				"PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>\r\n" + 
				"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\r\n" + 
				"PREFIX ta: <https://eopen-project.eu/ontologies/tweet-annotations#>\r\n" + 
				"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\r\n" + 
				"PREFIX oa: <http://www.w3.org/ns/oa#>\r\n" + 
				"PREFIX time: <https://www.w3.org/2006/time#> "+
				"select ?distance ?area where {\r\n" + 
				"     ?a ta:hasPolygon ?s.\r\n" + 
				"    ?a ta:hasArea ?area.\r\n" + 
				"    ?s ogc:asWKT ?o .\r\n" + 
				"   \r\n" + 
				" \r\n" + 
				//"BIND(geof:distance(?o, '''<http://www.opengis.net/def/crs/OGC/1.3/CRS84>\r\n" + 
				//"            Polygon (("+polygon+"))'''^^geo:wktLiteral, uom:metre) AS ?distance)\r\n" + 
				"BIND(geof:distance(\"<http://www.opengis.net/def/crs/EPSG/0/4326> MULTIPOLYGON ((("+polygon+")))\"^^<http://www.opengis.net/ont/geosparql#wktLiteral>, ?o) as ?distance)"
				+"    FILTER(?distance=\"0.0\"^^xsd:double)\r\n" + 
				"   FILTER(xsd:double(?ts)>=\""+start+"\"^^xsd:double && xsd:double(?ts)<=\""+end+"\"^^xsd:double)\r\n" + 
				
				"} \r\n" + 
				"\r\n" + 
				"";*/
	        
	        query="PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>\r\n" + 
					"PREFIX ogc: <http://www.opengis.net/ont/geosparql#>\r\n" + 
					"PREFIX geof: <http://www.opengis.net/def/function/geosparql/>\r\n" + 
					"PREFIX geo: <http://www.opengis.net/ont/geosparql#>\r\n" + 
					"PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>\r\n" + 
					"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\r\n" + 
					"PREFIX ta: <https://eopen-project.eu/ontologies/tweet-annotations#>\r\n" + 
					"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\r\n" + 
					"PREFIX oa: <http://www.w3.org/ns/oa#>\r\n" + 
					"PREFIX time: <https://www.w3.org/2006/time#> "+
					"select * where {\r\n" + 
					"     ?fl oa:hasTarget ?a.\r\n" + 
					"    ?a ta:hasPolygon ?s.\r\n" + 
					"    ?a rdfs:label ?lbl.\r\n" + 
					"    ?a ta:hasFloodedArea ?flooded_area.\r\n" + 
					"    ?a ta:hasPercentage ?perc.\r\n" + 
					"    ?a ta:hasArea ?area.\r\n" + 
					"    ?s ogc:asWKT ?o .\r\n" + 
					"\r\n" + 
					"    ?fl oa:hasBody ?b.\r\n" + 
					"    ?b ta:comesFrom ?sent.\r\n" + 
					"    ?b ta:hasDate ?ts. \r\n" + 
					"    ?b ta:hasFloodMapFile ?flood_map.\r\n" + 
					"    ?b ta:isFlooded ?flooded.\r\n" + 
					"   \r\n" + 
					" \r\n" + 
					//"BIND(geof:distance(?o, '''<http://www.opengis.net/def/crs/OGC/1.3/CRS84>\r\n" + 
					//"            Polygon (("+polygon+"))'''^^geo:wktLiteral, uom:metre) AS ?distance)\r\n" + 
					"BIND(geof:distance(\"<http://www.opengis.net/def/crs/EPSG/0/4326> MULTIPOLYGON ((("+polygon+")))\"^^<http://www.opengis.net/ont/geosparql#wktLiteral>, ?o) as ?distance)"
					+"    FILTER(?distance=\"0.0\"^^xsd:double)\r\n" + 
					"   FILTER(xsd:double(?ts)>=\""+start+"\"^^xsd:double && xsd:double(?ts)<=\""+end+"\"^^xsd:double)\r\n" + 
					
					"} \r\n" + 
					"\r\n" + 
					"";
		//flood area
		  query1 = twitterConnection.prepareTupleQuery(query);
		  System.out.println("flood::"+query);
	       
	        JSONArray fl = new JSONArray ();
	        JSONObject flood = new JSONObject ();
	        qresult1 = query1.evaluate();
	        JSONObject properties = new JSONObject();
	        JSONObject geom = new JSONObject();
	        while (qresult1.hasNext()) {
	            BindingSet st1 = qresult1.next();
	            if(st1.hasBinding("flood_map") && st1.hasBinding("ts") && st1.hasBinding("sent") && st1.hasBinding("flooded") && st1.hasBinding("area")
	            		&& st1.hasBinding("flooded_area") && st1.hasBinding("perc") && st1.hasBinding("o") && st1.hasBinding("lbl")) {
	            	flood = new JSONObject ();
	            	flood.put("type", "Feature");
	            	flood.put("id", st1.getValue("fl").stringValue().replaceAll("https://eopen-project.eu/ontologies/tweet-annotations#ChangeDetectionAnnotation_", ""));
	            	
	            	properties.put("flood_map", st1.getValue("flood_map").stringValue());
	            	properties.put("sensing_date", st1.getValue("ts").stringValue());
	            	properties.put("satellite_constellation", st1.getValue("sent").stringValue());
	            	properties.put("is_flooded", st1.getValue("flooded").stringValue());
	            	properties.put("whole_area_sq_meters", st1.getValue("area").stringValue());
	            	properties.put("flooded_sq_meters", st1.getValue("flooded_area").stringValue());
	            	properties.put("flood_percent", st1.getValue("perc").stringValue());
	            	
	            	String point = st1.getValue("o").stringValue();
	            	
	            	point = point.replaceAll("<http://www.opengis.net/def/crs/EPSG/0/4326> ", "");
	            	point = point.replaceAll("MULTIPOLYGON ", "");
	            	point = point.replaceAll("<http://www.opengis.net/ont/geosparql#wktLiteral>", "");
	            	point = point.replaceAll("[()]", "");
	            	point = point.replaceAll("[^\\x00-\\x7F]", "");
	            	point = point.replaceAll("^", "");
	            	point = point.replaceAll("&#94;","");
	            	JSONArray coords=new JSONArray();
	            	List<String> pointList = Arrays.asList(point.split(","));
	            	JSONArray allcoords=new JSONArray();
	            	String spec_point;
	            	for(int i=0; i<pointList.size(); i++) {
	            		spec_point = pointList.get(i);
	            		if(pointList.get(i).startsWith(" ")) {
	            			spec_point = spec_point.substring(1);
	            		}
	            		String[] splited = spec_point.split("\\s+");
	            		//System.out.println(spec_point);
	            	//	System.out.println(splited);
	            	//	System.out.println(splited.length);
	            		if(splited.length==2) {
	            			coords=new JSONArray();
		            		coords.add(splited[0].replaceAll("[^\\d.]", ""));
		            		coords.add(splited[1].replaceAll("[^\\d.]", ""));
		            		allcoords.add(coords);
		            	}
	            		
	            	}
	            	geom.put("type", "Polygon");
	            	geom.put("coordinates", allcoords);
	            	geom.put("geometry_name", st1.getValue("s").stringValue().replaceAll("https://eopen-project.eu/ontologies/tweet-annotations#", ""));
	            	flood.put("geometry", geom);
	            	
	            	properties.put("location_name", st1.getValue("lbl").stringValue());
	            	properties.put("feature_type", "contains_flooded_areas");
	            	flood.put("properties", properties);
	            	//fl.add(flood);   
	            	
	            	//result.put("contains_flooded_areas", fl);
	            	features.add(flood);
	            	flood = new JSONObject();
	            	properties = new JSONObject();
	            	geom = new JSONObject();
	            }
	        }
	        results.put("features", features);
	        return results.toString();
	}
}
